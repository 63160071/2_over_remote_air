/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lnw.remote_two_over;

/**
 *
 * @author Maintory_
 */
public class Remote {
    protected int temp;
    protected int fan;
    protected char input;
    public Remote(int temp,int fan){
        this.temp = temp;
        this.fan = fan;
    }
    public void turnOn(int temp,int fan){
        System.out.println("Beep !");
        System.out.println("\\\\-----------------------------------//");
        System.out.println("   Type U to tempup D to temp down !");
        System.out.println("   Type F to increase fanpower");
                System.out.println("   Press Q to Exit");
        System.out.println("//-----------------------------------\\\\");
        getTemp();
        }
    public void showInfo(){
        System.out.println("Temp: " + getTemp() +" Celcius");
    }
    public int setTemp(int temp){
        this.temp = temp;
        return temp;
    }
    public int setFan(int fan){
        this.fan=fan;
        return fan;
    }
    public int getFan(){
        return fan;
    }
    public int getTemp(){
        return temp;
    }
}
