/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lnw.remote_two_over;

/**
 *
 * @author Maintory_
 */
public class Panel extends Remote{ // Overriding from remote class
    public Panel(int temp,int fan){
        super(temp,fan);
    }
    public void upTemp(){
        setTemp(getTemp()+1);
    }
    public void upFan(){
        setFan(getFan()+1);
    }
        //------------------------------------------------------------------//
                              // Print fan algorithm //
        // Main idea if the fanpower exceeds 3 it will return to 1 at a time//
      //-----------------------------------------------------------------------//
    public boolean printFan(){
        if(getFan() == 1){
            System.out.println("Fanpower: |");
        }
        else if(getFan() == 2){
            System.out.println("Fanpower: ||");
        }
        else if(getFan() == 3){
            System.out.println("Fanpower: |||");
        }
        else{
            setFan(1);
            System.out.println("Fanpower: |");
        }
        return true;
    }
    
}
