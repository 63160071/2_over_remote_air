/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lnw.remote_two_over;

/**
 *
 * @author Maintory_
 */
public class Control extends Panel {
        //------------------------------------------------------------------//
                            // INHERITANCE FROM Panel CLASS //
       //------------------------------------------------------------------//
    public Control(int temp, int fan) {
        super(temp, fan);
    }
        //------------------------------------------------------------------//
               // USE showInfo() to introduce the control pannel //
       //------------------------------------------------------------------//
    @Override
    public void showInfo() {
        System.out.println("Temp: " + getTemp() + " Celcius");
        printFan();
    }
        //------------------------------------------------------------------//
                      // Input value in overloading method //
       //------------------------------------------------------------------//
    public void change(char input) {
        this.input = input;
        switch (input) {
            case 'D':
            case 'd':
                if (getTemp() <= 18) {
                    System.out.println("Temp cannot go below 18");
                    break;
                } else {
                    setTemp(getTemp() - 1);
                }
                break;
            case 'U':
            case 'u':
                if (getTemp() >= 30) {
                    System.out.println("Temp cannot go abobe 30");
                    break;
                } else {
                    setTemp(getTemp() + 1);
                }
                break;
            case 'F':
            case 'f':
                setFan(getFan() + 1);
                break;
            default:
                System.out.println("\\\\-----------------------------------//");
                System.out.println("   Type U to tempup D to temp down !");
                System.out.println("   Type F to increase fanpower");
                System.out.println("   Press Q to Exit");
                System.out.println("//-----------------------------------\\\\");
        }
    }
}
        //------------------------------------------------------------------//
                                     // END //
       //------------------------------------------------------------------//